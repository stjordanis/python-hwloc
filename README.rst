=================================
Python 2 and 3 bindings for hwloc
=================================

I have retired and am no longer maintaining or developing this. I could work
on it more if anyone is interested in using it.

If there is someone who would like to take it over, please contact me.

Fedora and centOS packages are available at
`COPR <https://copr.fedorainfracloud.org/coprs/streeter/python-hwloc/>`_.


Example pip install from git source with virtualenv on Debian9:

Debian9, Python2:

.. code:: bash

   sudo apt-get update
   sudo apt-get install build-essential virtualenv libpython2.7-dev \
      libnuma-dev libhwloc-dev libibverbs-dev
   virtualenv -p python2 p2pip
   cd p2pip
   source bin/activate
   pip install Cython Babel
   pip install python2-libnuma
   pip install python2-hwloc
   # try it!
   python2 -c "import hwloc;print hwloc.version_string()"

Python3 on Debian9:

.. code:: bash

   sudo apt-get update
   sudo apt-get install build-essential virtualenv libpython3.5-dev \
      libnuma-dev libhwloc-dev libibverbs-dev
   virtualenv -p python3 p3pip
   cd p3pip
   source bin/activate
   pip install Cython Babel
   pip install python2-libnuma
   pip install python2-hwloc
   # try it!
   python3 -c "import hwloc;print(hwloc.version_string())"
