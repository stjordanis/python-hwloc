#!/usr/bin/env python3
# -*- python -*-

#
# Copyright (C) 2011-2017 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of the hwloc-bind utility
# from the hwloc package. This is simply an example of using the
# python bindings for hwloc, and is not intended as a replacement for
# the original utility.
#

from __future__ import print_function
import hwloc
import sys, getopt, os

from python_hwloc_locations import process_location_args

MembindPolicyString = { hwloc.MEMBIND_DEFAULT: 'default',
                        hwloc.MEMBIND_FIRSTTOUCH: 'firsttouch',
                        hwloc.MEMBIND_BIND: 'bind',
                        hwloc.MEMBIND_INTERLEAVE: 'interleave',
                        hwloc.MEMBIND_REPLICATE: 'replicate',
                        hwloc.MEMBIND_NEXTTOUCH: 'nexttouch',
                        }
        
class MemPolicyError(Exception):
    def __init__(self, string):
        self.msg = string
    def __str__(self):
        return 'Unrecognized memory policy: ' + self.msg

class MemPolicyType():
    _names = dict([(v, k) for (k, v) in MembindPolicyString.items()])
    def __init__(self, string):
        for i in range(2, len(string)+2):
            match = [s for s in list(self._names.keys()) if i-2 < len(s) and s[:i].lower() == string[:i].lower()]
            if len(match) == 1 and string == match[0][:len(string)]:
                try:
                    self.type = self._names[match[0]]
                    break
                except KeyError:
                    pass
        else:
            raise MemPolicyError(string)

    def __int__(self):
        return self.type

class CpuMemError(Exception):
    def __init__(self, string):
        self.msg = string
    def __str__(self):
        return 'Unrecognized get option: ' + self.msg

class CpuMemString():
    _names = ('cpu', 'memory', 'both')
    def __init__(self, string):
        string = string.lower()
        for i in range(1, len(string)+1):
            match = [s for s in self._names if i-1 < len(s) and s[:i] == string[:i]]
            if len(match) == 1 and string == match[0][:len(string)]:
                self.string = match[0]
                break
        else:
            raise CpuMemError(string)

    def __str__(self):
        return self.string


progname = sys.argv[0]
def usage(where):
    print('Usage:', progname, '''[options] <location> -- command ...
 <location> may be a space-separated list of cpusets or objects
            as supported by the hwloc-calc utility.
Options:
  --cpubind=<location>  Use following arguments for cpu binding (default)
  --membind=<location>  Use following arguments for memory binding
  --mempolicy=default|firsttouch|bind|interleave|replicate|nexttouch
                        Change the memory binding policy (default is bind)
  -l --logical          Take logical object indexes (default)
  -p --physical         Take physical object indexes
  --single              Bind on a single CPU to prevent migration
  --strict              Require strict binding
  --get=cpu|memory|both Retrieve current process binding
  --get-last-cpu-location
                 Retrieve the last processors where the current process ran
  --pid=<pid>           Operate on process <pid>
  --taskset             Manipulate taskset-specific cpuset strings
  -v --verbose          Show verbose messages
  --version             Report version and exit''', file=where)

try:
    options, args = getopt.getopt(sys.argv[1:], 'lpv?',
                                  ['cpubind=', 'membind=',
                                   'mempolicy=',
                                   'logical', 'physical',
                                   'single',
                                   'strict',
                                   'get=',
                                   'get-last-cpu-location',
                                   'pid=',
                                   'taskset',
                                   'verbose',
                                   'version', 'help',])
except getopt.GetoptError as err:
    print(str(err), file=sys.stderr)
    usage(sys.stderr)
    sys.exit(2)

topo = hwloc.Topology()
topo.set_flags(hwloc.TOPOLOGY_FLAG_WHOLE_IO)
topo.load()

get_binding = None
get_last_cpu_location = False
single = False
verbose = False
logical = True
taskset = False
cpubind_flags = 0
membind_policy = int(MemPolicyType('bind'))
membind_flags = 0
pid = None

cpubind_set = hwloc.Bitmap.alloc()
membind_set = hwloc.Bitmap.alloc()

for o, a in options:
    if o == '--version':
        print(progname, hwloc.version_string())
        sys.exit(0)
    elif o in ('-v', '--verbose'):
        verbose = True
    elif o in ('-?', '--help'):
        usage(sys.stdout)
        sys.exit(0)
    elif o == '--single':
        single = True
    elif o == '--strict':
        cpubind_flags = hwloc.CPUBIND_STRICT
        membind_flags = hwloc.MEMBIND_STRICT
    elif o == '--pid':
        try:
            pid = int(a, 10)
        except ValueError as err:
            print(str(err), '\n', file=sys.stderr)
            usage(sys.stderr)
            sys.exit(2)
    elif o in ('-l', '--logical'):
        logical = True
    elif o in ('-p', '--physical'):
        logical = False
    elif o == '--taskset':
        taskset = True
    elif o == '--get-last-cpu-location':
        get_last_cpu_location = True
    elif o == '--get':
        try:
            get_binding = str(CpuMemString(a))
        except CpuMemError as err:
            print(str(err), '\n', file=sys.stderr)
            usage(sys.stderr)
            sys.exit(2)
    elif o == '--cpubind':
        cpubind_set = process_location_args(topo, a.split(' '), logical, taskset, verbose)
        if cpubind_set is None:
            print('cpu location syntax error:', a, file=sys.stderr)
            sys.exit(2)
    elif o == '--membind':
        membind_set = process_location_args(topo, a.split(' '), logical, taskset, verbose)
        if membind_set is None:
            print('memory location syntax error:', a, file=sys.stderr)
            sys.exit(2)
    elif o == '--mempolicy':
        try:
            membind_policy = int(MemPolicyType(a))
        except MemPolicyError as err:
            print(str(err), file=sys.stderr)
            usage(sys.stderr)
            sys.exit(2)

if get_binding or get_last_cpu_location:
    if get_binding in ('cpu', 'both'):
        if get_last_cpu_location:
            if pid:
                try:
                    cpubind_set = topo.get_proc_last_cpu_location(pid, 0)
                except OSError as err:
                    print('get_proc_last_cpu_location for pic % failed (errno %d %s)' % (pid, err.errno, str(err)), file=sys.stderr)
                    sys.exit(err.errno)
            else:
                try:
                    cpubind_set = topo.get_last_cpu_location(0)
                except OSError as err:
                    print('get_last_cpu_location failed (errno %d %s)' % (err.errno, str(err)), file=sys.stderr)
        else:
            if pid:
                try:
                    cpubind_set = topo.get_proc_cpubind(pid, 0)
                except OSError as err:
                    print('get_proc_cpubind for pid %d failed (errno %d %s)' % (pid, err.errno, str(err)), file=sys.stderr)
                    sys.exit(err.errno)
            else:
                try:
                    cpubind_set = topo.get_cpubind(0)
                except OSError as err:
                    print('get_cpubind failed (errno %d %s)' % (err.errno, str(err)), file=sys.stderr)
                    sys.exit(err.errno)
        if taskset:
            print(cpubind_set.taskset_asprintf())
        else:
            print(str(cpubind_set))
    if get_binding in ('memory', 'both'):
        if pid:
            try:
                membind_set, policy = topo.get_proc_membind(pid, 0)
            except OSError as err:
                print('get_proc_membind for pid %d failed (errno %d %s)' % (pid, err.errno, str(err)), file=sys.stderr)
                sys.exit(err.errno)
        else:
            try:
                membind_set, policy = topo.get_membind(0)
            except OSError as err:
                print('get_membind failed (errno %d %s)' % (err.errno, str(err)), file=sys.stderr)
                sys.exit(err.errno)
        if taskset:
            s = membind_set.taskset_asprintf(),
        else:
            s = str(membind_set),
        try:
            s = s+' ('+MembindPolicyString[policy]+')'
        except KeyError:
            print('unknown memory policy', policy, file=sys.stderr)
            assert False
        print(s)
    sys.exit(0)

if not membind_set.iszero:
    if verbose:
        print('binding on memory set', str(membind_set), file=sys.stderr)
    if single:
        membind_set.singlify()
    if pid:
        try:
            topo.set_proc_membind(pid, membind_set, membind_policy, membind_flags)
        except OSError as err:
            print('set_proc_membind %s for pid %d failed (errno %d %s)' % (str(membind_set), pid, err.errno, str(err)), file=sys.stderr)
            sys.exit(err.errno)
    else:
        try:
            topo.set_membind(membind_set, membind_policy, membind_flags)
        except OSError as err:
            print('set_membind %s failed (errno %d %s)' % (str(membind_set), err.errno, str(err)), file=sys.stderr)

if not cpubind_set.iszero:
    if verbose:
        print('binding on cpu set', str(cpubind_set), file=sys.stderr)
    if single:
        cpubind_set.singlify()
    if pid:
        try:
            topo.set_proc_cpubind(pid, cpubind_set, cpubind_flags)
        except OSError as err:
            print('set_proc_cpubind %s for pid %d failed (errno %d %s)' % (str(cpubind_set), pid, err.errno, str(err)), file=sys.stderr)
            sys.exit(err.errno)
    else:
        try:
            topo.set_cpubind(cpubind_set, cpubind_flags)
        except OSError as err:
            print('set_cpubind % failed (errno %d %s)' % (str(cpubind_set), err.errno, str(err)), file=sys.stderr)
            sys.exit(err.errno)

if pid:
    sys.exit(0)
elif args:
    try:
        os.execvp('/bin/sh', ['/bin/sh', '-c', ' '.join(args)])
    except OSError as err:
        print('%s:: Failed to launch executable "%s"' %(progname, args[0]), file=sys.stderr)
        print(str(err))
        sys.exit(err.errno)
else:
    print('%s: nothing to do!' % progname, file=sys.stderr)
    sys.exit(2)
