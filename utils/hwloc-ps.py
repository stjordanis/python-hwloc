#!/usr/bin/env python3
# -*- python -*-

#
# Copyright (C) 2011-2017 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of the hwloc-ps utility
# from the hwloc package. It is NOT complete.
#

from __future__ import print_function
import sys, getopt, os
import hwloc

progname = sys.argv[0]
def usage():
    print('List currently-running processes that are bound', file=sys.stderr)
    print('Usage:', progname, '[ options ] ...', file=sys.stderr)
    print('Options', file=sys.stderr)
    print('  -a --all       Show all processes, including those that are not bound', file=sys.stderr)
    print('  -l --logical   Use logical object indexes (default)', file=sys.stderr)
    print('  -p --physical  Use physical object indexes', file=sys.stderr)
    print('  -c --cpuset    Show cpuset instead of objects', file=sys.stderr)
    print('  -t --threads   Show threads', file=sys.stderr)
    print('  --whole-system Do not consider administration limitations', file=sys.stderr)

try:
    options = getopt.getopt(sys.argv[1:], 'alpct', ['all', 'logical', 'physical', 'cpuset', 'threads', 'whole-system'])[0]
except getopt.GetoptError as err:
    print(str(err))
    usage()
    sys.exit(2)

show_all = False
logical = True
show_cpuset = False
show_threads = False
whole_system = False

for o, a in options:
    if o in ('-a', '--all'):
        show_all = True
    elif o in ('-l', '--logical'):
        logical = True
    elif o in ('-p', '--physical'):
        logical = False
    elif o in ('-c', '--cpuset'):
        show_cpuset = True
    elif o in ('-t', '--threads'):
        show_threads = True
    elif o == '--whole-system':
        whole_system = True

def print_task(task_pid, task_name, task_cpuset, thread):
    if thread:
        print(' ', end=' ')
    print('%ld\t' % task_pid, end=' ')
    if show_cpuset:
        print(str(task_cpuset), end=' ')
    else:
        remaining = cpuset.dup()
        first = True
        while not remaining.iszero:
            obj = topo.get_first_largest_obj_inside_cpuset(remaining)
            idx = obj.os_index
            if logical:
                idx = obj.logical_index
            if not first:
                print(' ', end=' ')
            print(obj.type_asprintf(True), end=' ')
            if idx != hwloc.INT_MAX:
                print('%u' % idx, end=' ')
            remaining = remaining.andnot(obj.cpuset)
    print('\t\t' + task_name)

topo = hwloc.Topology()
if whole_system:
    topo.set_flags(hwloc.TOPOLOGY_FLAG_WHOLE_SYSTEM)
topo.load()

support = topo.support

if not support.cpubind.get_thisproc_cpubind:
    sys.exit(0)

topocpuset = topo.cpuset

for pidfile in os.listdir('/proc'):
    try:
        pid = int(pidfile)
    except:
        pid = 0
    if not pid:
        continue

    with open('/proc/%s/cmdline' % (pidfile,)) as f:
        name = f.read().strip()
    if not len(name):
        continue

    name = name.split('\0')[0]

    tids = []
    boundthreads = 0
    if show_threads:
        tidcpusets = []
        tidstrings = os.listdir('/proc/%s/task' % pidfile)
        if len(tidstrings) > 1:
            for tname in tidstrings:
                try:
                    t = int(tname)
                except:
                    continue
                cpuset = topo.linux_get_tid_cpubind(t)
                cpuset &= topocpuset
                tids.append(t)
                tidcpusets.append(cpuset.dup())
                if cpuset.iszero:
                    continue
                if cpuset == topocpuset and not show_all:
                    continue
                boundthreads += 1

    try:
        cpuset = topo.get_proc_cpubind(pid, 0)
    except:
        continue

    cpuset &= topocpuset
    if cpuset.iszero:
        continue

    if cpuset == topocpuset and (len(tids) > 1 or boundthreads == 0) and not show_all:
        continue

    print_task(pid, name, cpuset, False)
    if len(tids) > 1:
        for i in range(len(tids)):
            print_task(tids[i], name, tidcpusets[i], True)
