# -*- python -*-

#
# Copyright (C) 2011-2017 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# location parsing helpers for hwloc-calc.py and hwloc-bind.py
#

from __future__ import print_function
import hwloc, sys

class LocationSyntaxError(Exception):
	def __init__(self, string):
		self.msg = string
	def __str__(self):
		return 'Invalid syntax: ' + self.msg

locations_usage = """\n	core:2-3		for the second and third core
	node:1.pu:2	   the third PU of the second NUMA node
	0x12345678		a CPU set given a bitmask string
	os=eth0		   the operating system device named eth0
	pci=0000:01:02.0  the PCI device with the given bus ID
  with prefix ~ to remove, ^ for xor and x for intersection
  (see Location Specification in hwloc(7) for details)."""

_logical = False
_taskset = False
_verbose = False
_topo = None

def mask_append_add(oldset, newset):
	if _verbose:
		print('adding', str(newset), 'to', str(oldset), file=sys.stderr)
	return oldset | newset

def mask_append_clr(oldset, newset):
	if _verbose:
		print('clearing', str(newset), 'from', str(oldset), file=sys.stderr)
	return oldset.andnot(newset)

def mask_append_and(oldset, newset):
	if _verbose:
		print("and'ing", str(newset), 'from', str(oldset), file=sys.stderr)
	return oldset & newset

def mask_append_xor(oldset, newset):
	if _verbose:
		print("xor'ing", str(newset), 'from', str(oldset), file=sys.stderr)
	return oldset ^ newset

mask_append_ops = { '~': mask_append_clr,
					'x': mask_append_and,
					'^': mask_append_xor,
					}

def depth_of_location(string):
	"""Match a string to an Object type. The string needs only be long
	enough to be unique, but must match for as much as is supplied.
	If the string is 'L?cache' or 'Group?', parse the supplied number.
	Return the depth in the topology and the type value of the specified
	object type."""
	_names = ('system', 'machine', 'misc', 'numanode', 'node', 'socket', 'core', 'pu', 'pci', 'os')
	level = None
	myType = None
	save = string
	string = string.lower()
	for i in range(2, len(string)+2):
		match = [s for s in _names if i-2 < len(s) and s[:i] == string[:i]]
		if len(match) == 1 and string == match[0][:len(string)]:
			myType = hwloc.Obj.type_of_string(match[0])
			break
	if not myType:
		# with depth?
		# LNcache
		if string[:1] == 'l' and string.endswith('cache'):
			try:
				level = int(string[1:-len('cache')])
				myType = hwloc.Obj.type_of_string('cache')
			except:
				pass
		if not myType:
			# GroupN
			if string.startswith('group'):
				try:
					level = int(string[len('group'):])
					myType = hwloc.Obj.type_of_string('group')
				except:
					pass
		if not myType:
			if _verbose:
				print(save, 'is not a valid type specification', file=sys.stderr)
			raise LocationSyntaxError(save)
		if level:
			# matched a type with a depth attribute, look at the first object
			# of each level to find the depth
			assert myType == hwloc.OBJ_CACHE or myType == hwloc.OBJ_GROUP
			i = 0
			while True:
				try:
					obj = _topo.get_obj_by_depth(i, 0)
				except:
					if _verbose:
						print('type %s with custom depth %d does not exist' % (hwloc.Obj.string_of_type(myType), i), file=sys.stderr)
					raise LocationSyntaxError(save)
				if obj.type == myType and (
					( myType == hwloc.OBJ_CACHE and level == obj.attr.cache.depth )
					or ( myType == hwloc.OBJ_GROUP and level == obj.attr.group.depth )
					):
					return i, myType
				i += 1
	return _topo.get_type_or_above_depth(myType), myType

def parse_index_range(string, width):
	"""Return the starting and ending values of an index range, and
	the stepping of the index value."""
	if string == 'all':
		return list(range(0, width))
	if string == 'odd':
		return list(range(1, width, 2))
	if string == 'even':
		return list(range(0, width, 2))
	if ':' in string:
		if not _logical and _verbose:
			print('WARNING: index range syntax with physical indexes may produce unexpected results.', file=sys.stderr)
		start, length = string.split(':', 1)
		try:
			start = int(start)
			length = int(length)
		except:
			if _verbose:
				print('Invalid range syntax', string, file=sys.stderr)
			raise LocationSyntaxError(string)
		if start+length <= width:
			return list(range(start, start+length))
		return list(range(start, width)) + list(range(start+length-width))
	elif '-' in string:
		start, end = string.split('-', 1)
		try:
			start = int(start)
			if not len(end):
				end = start+width
			else:
				end = int(end)
			assert start <= end
		except:
			if _verbose:
				print('Invalid range syntax', string, file=sys.stderr)
			raise LocationSyntaxError(string)
		return list(range(start, end+1))
	else:
		return (int(string),)

def pci_by_vendor_and_device(vendor, device):
	for obj in _topo.pcidevs:
		if (vendor == -1 or vendor == obj.attr.pcidev.vendor_id) \
				and (device == -1 or device == obj.attr.pcidev.device_id):
			yield obj

def pci_device_range(string):
	try:
		vendor, device, idx = string.split(':')[1:]
	except:
		try:
			vendor, device = string.split(':')[1:]
			idx = 'all'
		except:
			raise LocationSyntaxError(string)
	try:
		vendor = int(vendor, 16)
		device = int(device, 16)
	except:
		raise LocationSyntaxError(string)
	depth = _topo.get_type_depth(hwloc.OBJ_PCI_DEVICE)
	width = _topo.get_nbobjs_by_depth(depth)
	indexes = parse_index_range(idx, width)
	idx = 0
	yielded = False
	for obj in pci_by_vendor_and_device(vendor, device):
		if idx in indexes:
			if _verbose:
				print('using matching PCI object #%d bus id %04x:%02x:%02x.%01x' % (idx, obj.attr.pcidev.domain, obj.attr.pcidev.bus, obj.attr.pcidev.dev, obj.attr.pcidev.func))
			yield _topo.get_non_io_ancestor_obj(obj)
			yielded = True
		idx += 1
	if not yielded:
		print('no matching %s PCI object index' % (string,), indexes, file=sys.stderr)

def objects_at_location(string, cover):
	"""Parse an argument intended to specify an Object or set of objects.
	This can take the forms described in hwloc(7) under
	"Location Specification" and supports the Object and Index
	syntax described in the same man page. Dotted chains are not handled here.
	Generate a list of matching objects."""
	# look for object:index format
	try:
		obj, idx = string.split(':', 1)
	except:
		obj = string
		idx = 'all'
	depth = depth_of_location(obj)[0]
	width = _topo.get_nbobjs_by_depth(depth)
	indexes = parse_index_range(idx, width)
	offset = None
	yielded = False
	for obj in _topo.objs_by_depth(depth):
		if not obj.cpuset in cover:
			continue
		if _logical:
			if offset is None:
				offset = obj.logical_index
			number = obj.logical_index - offset
		else:
			if offset is None:
				offset = obj.os_index
			number = obj.os_index - offset
		if number in indexes:
			if _verbose:
				print('using object #%u depth %u below cpuset %s' % (number, depth, cover))
			yielded = True
			yield obj
	if not yielded:
		#print('object %s depth %d (type %s) below cpuset %s does not exist' % (indexes, depth, hwloc.Obj.string_of_type(_topo.get_depth_type(depth)), cover), file=sys.stderr)
		pass

def parse_argument(string):
	"""Parse an argument intended to specify a CPU bitmask.
	This can take the forms described in hwloc(7) under
	"Location Specification" and supports the Object and Index
	syntax described in the same man page."""
	# look for pci|os format
	tmp = string.lower()
	if tmp.startswith('pci='):
		spec = tmp.split('=', 1)[1]
		try:
			obj = _topo.get_pcidev_by_busidtmp(spec)
		except hwloc.ArgError as e:
			print(str(e), spec)
			obj = None
		if not obj:
			print('invalid PCI device', spec, file=sys.stderr)
			return hwloc.Bitmap.alloc()
		return _topo.get_non_io_ancestor_obj(obj).cpuset
	if tmp.startswith('os='):
		spec = tmp.split('=', 1)[1]
		for obj in _topo.osdevs:
			if spec == obj.name:
				return _topo.get_non_io_ancestor_obj(obj).cpuset
		print('invalid OS device', spec, file=sys.stderr)
		return hwloc.Bitmap.alloc()
	cover = _topo.root_obj.cpuset
	newset = hwloc.Bitmap.alloc()
	# look for pci:vendor:device:index
	if tmp.startswith('pci:'):
		for obj in pci_device_range(tmp):
			newset |= obj.cpuset
		return newset
	if tmp.startswith('0x'):
		taskset = not ',' in tmp
		inf = '0xf....f,'
		if taskset:
			inf = '0xf....f'
		if tmp.startswith(inf):
			tmp = tmp[len(inf):]
		if False in [ c in '0123456789abcdefx,' for c in tmp ]:
			raise LocationSyntaxError(string)
		if taskset:
			newset.taskset_sscanf(tmp)
			return newset
		else:
			return hwloc.Bitmap.alloc(tmp)
	if '.' in tmp:
		objs = None
		for chain in tmp.split('.'):
			if not objs:
				objs = objects_at_location(chain, cover)
			else:
				sub_objs = []
				for o in objs:
					sub_objs += objects_at_location(chain, o.cpuset)
				objs = sub_objs
		for obj in objs:
			newset |= obj.cpuset
		return newset
	for o in objects_at_location(tmp, cover):
		newset |= o.cpuset
	return newset

def process_arg(string, _prev):
	if string in ('all', 'root'):
		return  _topo.root_obj.cpuset
	return parse_argument(string)

def process_location_args(topology, args, logical, taskset, verbose):
	global _topo, _logical, _taskset, _verbose
	_topo = topology
	_logical = logical
	_taskset = taskset
	_verbose = verbose
	prev = hwloc.Bitmap.alloc()
	for s in args:
		func = mask_append_add
		try:
			func = mask_append_ops[s[0]]
			s = s[1:]
		except:
			pass
		prev = func(prev, process_arg(s, prev))
	return prev
