#! /usr/bin/env python3
'''
Created on Dec 17, 2014

@author: streeter
'''
from __future__ import print_function
import sys

from gi import require_version as gi_require_version
gi_require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio, GLib, GObject

from socket import gethostname
from datetime import datetime
import argparse
import hwloc
import os


class TitleGrid(Gtk.Grid):
	def __init__(self):
		Gtk.Grid.__init__(self, visible=True, row_homogeneous=True,
						  orientation='vertical')
		self.hostname = Gtk.Label(visible=False, no_show_all=True)
		self.add(self.hostname)
		self.indexes = Gtk.Label(visible=True)
		self.add(self.indexes)
		self.date = Gtk.Label(datetime.today().strftime('Date: %c'),
							  visible=True)
		self.add(self.date)

	def update(self, topology, options):
		if topology.is_thissystem:
			self.hostname.set_text('Host: ' + gethostname())
			self.hostname.show()
		if options['logical']:
			self.indexes.set_text('Indexes: logical')
		else:
			self.indexes.set_text('Indexes: physical')
		self.date.set_text(datetime.today().strftime('Date: %c'))

class GearsMenu(Gio.Menu):
	def __init__(self):
		Gio.Menu.__init__(self)
		self.append(label='_Logical indexes',
					detailed_action='win.show-logical')

class GearsButton(Gtk.Alignment):
	def __init__(self):
		Gtk.Alignment.__init__(self, left_padding=4, xalign=1.0, yalign=0.5,
							   xscale=0.0, yscale=0.0,
							   visible=True)
		self.menu_button = Gtk.MenuButton(visible=True)
		self.menu_button.set_image(Gtk.Image(icon_name='emblem-system-symbolic',
											 icon_size=1, visible=True))
		self.menu_button.set_menu_model(GearsMenu())
		self.add(self.menu_button)

class MainHeader(Gtk.HeaderBar):
	def __init__(self):
		Gtk.HeaderBar.__init__(self, visible=True)
		try:
			self.set_has_subtitle(False)
		except AttributeError:
			pass
		self.title_grid = TitleGrid()
		self.set_custom_title(self.title_grid)
		self.pack_end(GearsButton())

	def update(self, topology, options):
		self.title_grid.update(topology, options)

def _obj_string(obj, logical, verbose=0):
	s = obj.type_asprintf(verbose)
	if obj.depth != 0:
		if logical:
			s += ' L#' + str(obj.logical_index)
		else:
			if obj.os_index != hwloc.UINT_MAX:
				s += ' P#' + str(obj.os_index)
	attr = obj.attr_asprintf(' ', verbose)
	if attr:
		s += ' (' + attr + ')'
	return s

class Tree(Gtk.ScrolledWindow):
	def __init__(self, topology, options):
		Gtk.ScrolledWindow.__init__(self, visible=True,
									hscrollbar_policy=Gtk.PolicyType.AUTOMATIC,
									vscrollbar_policy=Gtk.PolicyType.AUTOMATIC)
		self.options = options
		self.topology = topology
		self.tree_store = Gtk.TreeStore(*(GObject.TYPE_STRING,
										  GObject.TYPE_STRING))
		self._recurse_children(None, topology.root_obj)
		self.tree_view = Gtk.TreeView(self.tree_store)
		r = Gtk.CellRendererText()
		c = Gtk.TreeViewColumn(title='Object', cell_renderer=r)
		c.add_attribute(r, 'text', 0)
		self.tree_view.append_column(c)
		c = Gtk.TreeViewColumn(title='Description', cell_renderer=r)
		c.add_attribute(r, 'text', 1)
		self.tree_view.append_column(c)
		self.tree_view.expand_all()
		self.tree_view.show()
		self.add(self.tree_view)
		self.show_all()
		
	def _recurse_children(self, parent, obj):
		if obj.depth < 0:
			return
		it = self.tree_store.append(parent)
		self.tree_store.set(it, 0, obj.type_string)
		self.tree_store.set(it, 1, _obj_string(obj, self.options['logical']))
		for c in obj.children:
			self._recurse_children(it, c)

	def update(self):
		print('main frame update', self.options)
		self.tree_store.clear()
		self._recurse_children(None, self.topology.root_obj)
		self.tree_view.expand_all()

class MainGrid(Gtk.Grid):
	def __init__(self, options):
		Gtk.Grid.__init__(self, visible=True, column_homogeneous=True,
						  row_homogeneous=False,
						  orientation=Gtk.Orientation.VERTICAL)
		self.options = options
		self.topology = hwloc.Topology()
		form = options['input_format']
		if options['input'] is not None:
			if not form:
				if os.path.isfile(options['input']):
					form = 'xml'
				elif os.path.isdir(options['input']):
					form = 'fsroot'
				else:
					form = 'synthetic'
			if form == 'xml':
				if options['input'] == '-':
					options['input'] = '/dev/stdin'
				self.topology.set_xml(options['input'])
			elif form == 'fsroot':
				self.topology.set_fsroot(options['input'])
			else:
				self.topology.set_synthetic(options['input'])
		options['input_format'] = form
		if options['ignore'] is not None:
			for a in options['ignore']:
				try:
					self.topology.ignore_type(a)
				except hwloc.ArgError as e:
					print(str(e), file=sys.stderr)
		flags = hwloc.TOPOLOGY_FLAG_IO_DEVICES | hwloc.TOPOLOGY_FLAG_IO_BRIDGES | hwloc.TOPOLOGY_FLAG_ICACHES
		if options['no_icaches']:
			flags &= ~hwloc.TOPOLOGY_FLAG_ICACHES
		if options['whole_system']:
			flags &= ~hwloc.TOPOLOGY_FLAG_WHOLE_SYSTEM
		if options['no_io']:
			flags &= ~(hwloc.TOPOLOGY_FLAG_IO_DEVICES | hwloc.TOPOLOGY_FLAG_IO_BRIDGES)
		if options['no_bridges']:
			flags &= ~hwloc.TOPOLOGY_FLAG_IO_BRIDGES
		if options['whole_io']:
			flags |= hwloc.TOPOLOGY_FLAG_WHOLE_IO
		if options['thissystem']:
			flags |= hwloc.TOPOLOGY_FLAG_IS_THISSYSTEM
		self.topology.set_flags(flags)
		if options['no_useless_caches']:
			self.topology.ignore_type_keep_structure(hwloc.OBJ_CACHE)
		elif options['no_caches']:
			self.topology.ignore_type(hwloc.OBJ_CACHE)
		if options['merge']:
			self.topology.ignore_all_keep_structure()
		pid = options['pid']
		if pid is not None:
			if pid != 0:
				self.topology.set_pid(pid)
		self.topology.load()
		if options['restrict']:
			restrict = options['restrict']
			if restrict == 'binding':
				if pid is not None:
					restrict = self.topology.get_proc_cpubind(pid,
															  hwloc.CPUBIND_PROCESS)
				else:
					restrict = self.topology.get_cpubind(hwloc.CPUBIND_PROCESS)
			else:
				try:
					restrict = hwloc.Bitmap.alloc(restrict)
				except hwloc.ArgError as e:
					print(str(e), file=sys.stderr)
					restrict = None
			try:
				self.topology.restrict(restrict, 0)
			except:
				print('error restricting', file=sys.stderr)
		self.header = MainHeader()
		self.header.update(self.topology, options)
		self.add(self.header)
		self.center = Tree(self.topology, options)
		self.center.set_vexpand(True)
		self.add(self.center)
		self.center.update()
		
	def set_logical(self, state):
		if self.options['logical'] == state:
			return
		self.options['logical'] = state
		self.header.update(self.topology, self.options)
		self.center.update()

class Win(Gtk.ApplicationWindow):
	def __init__(self, options):
		Gtk.ApplicationWindow.__init__(self, title='lstopo')
		self.grid = MainGrid(options)
		simple_action = Gio.SimpleAction.new_stateful('show-logical', None,
													  GLib.Variant.new_boolean(options['logical']))
		simple_action.connect('activate', self.on_logical)
		self.add_action(simple_action)
		self.add(self.grid)

	def on_logical(self, action, arg):
		print('on_logical', repr(action), arg, action.get_state())
		toggle = not action.get_state()
		print(toggle)
		action.change_state(GLib.Variant.new_boolean(toggle))
		self.grid.set_logical(toggle)

class AppMenu(Gio.Menu):
	def __init__(self):
		Gio.Menu.__init__(self)
		item = Gio.MenuItem.new('_Quit', 'app.quit')
		item.set_attribute_value('accel', GLib.Variant('s', '<Ctrl>Q'))
		menu = Gio.Menu()
		menu.append_item(item)
		self.append_section(None, menu)

class App(Gtk.Application):
	def __init__(self):
		Gtk.Application.__init__(self,
								 flags=Gio.ApplicationFlags.NON_UNIQUE|Gio.ApplicationFlags.HANDLES_COMMAND_LINE)
		self.args = None

	def quit_activated(self, *args):
		self.quit()

	def do_startup(self):
		Gtk.Application.do_startup(self)
		
		self.set_app_menu(AppMenu())
		simple_action = Gio.SimpleAction(name='quit')
		simple_action.connect('activate', self.quit_activated)
		self.add_action(simple_action)

# handle-local-options isn't implemented until Fedora 21
#   def do_handle_local_options(self, args):
#		print(repr(args))
#		self.do_activate()
#		return -1

	def do_command_line(self, cmdline):
		Gtk.Application.do_command_line(self, cmdline)
		parser = argparse.ArgumentParser(prog='lstopo',
										 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
		ofo = parser.add_argument_group('Object filtering options')
		ofo.add_argument('--ignore', metavar='TYPE', action='append',
						 help='Ignore objects of the given type. Argument may be repeated')
		ofo.add_argument('--no-caches', action='store_true',
						 help='Do not show caches')
		ofo.add_argument('--no-useless-caches', action='store_true',
						 help='Do not show caches which do not have a hierarchical impact')
		ofo.add_argument('--no-icaches', action='store_true',
						 help='Do not show instruction caches')
		ofo.add_argument('--merge', action='store_true',
						 help='Do not show levels that do not have a hierarchical impact')
		ofo.add_argument('--restrict', metavar='CPUSET',
						 help='Restrict the topology to processors listed in CPUSET. Use "binding" as CPUSET to restrict to the current process binding')
		ofo.add_argument('--no-io', action='store_true',
						 help='Do not show any I/O device or bridge')
		ofo.add_argument('--no-bridges', action='store_true',
						 help='Do not any I/O bridge except hostbridges')
		ofo.add_argument('--whole-io', action='store_true',
						 help='Show all I/O devices and bridges')
		io = parser.add_argument_group('Input options')
		io.add_argument('-i', '--input',
						help='XML file, root directory, or synthetic string')
		io.add_argument('--if', '--input-format',
						choices=('xml', 'fsroot', 'synthetic'),
						dest='input_format',
						help='Enforce the input in the given format')
		io.add_argument('--thissystem', action='store_true',
						help='Assume that the input topology provides the topology for the system on which we are running')
		io.add_argument('--pid', type=int,
						help='Detect topology as seen by process PID')
		io.add_argument('--whole-system', action='store_true',
						help='Do not consider administration limitations')
		fo = parser.add_argument_group('Formatting options')
		fo.add_argument('-l', '--logical', action='store_true', default=False,
							help='Display hwloc logical instead of physical object indexes')
		parser.add_argument('--version', action='version', version='1.9')
		self.args = parser.parse_args(cmdline.get_arguments()[1:])
		if self.args.input_format:
			if not self.args.input:
				print(" '--input-format' requires '--input'", file=sys.stderr)
				parser.print_usage(sys.stderr)
				return -1
		self.do_activate()
		return 0

	def do_activate(self):
		win = Win(dict(**vars(self.args)))
		self.add_window(win)
		win.present()

if __name__ == '__main__':
	sys.exit(App().run(sys.argv))
