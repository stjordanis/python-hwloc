#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2011-2017 Red Hat, Inc.
#  This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#  This application is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  General Public License for more details.
#
# Authors:
#  Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of the lstopo utility
# from the hwloc package (only the gui part, only some options,
# and very linux-specific)
#

from __future__ import print_function

from gi import require_version as gi_require_version
gi_require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk

import hwloc, sys, getopt

from math import sqrt
from socket import gethostname
from datetime import datetime


def rectangle(nbobjs):
	"""Try to arrange a square-ish layout. Very empirical."""
	if nbobjs < 4:
		return nbobjs, 1
	sq = sqrt((nbobjs*4.0)/3.0)
	across = int(sq+0.5)
	sq = sqrt((nbobjs*3.0)/4.0)
	down = int(sq+0.5)
	if across*down < nbobjs:
		across += 1
	trail = nbobjs % across
	if	trail != 0 and trail < down:
		across += 1
		down -= 1
	return across, down

def delete_event(_widget, _event, _data=None):
	return False

def destroy(_widget, _data=None):
	Gtk.main_quit()

def keypress(_widget, event):
	if event.keyval in (Gdk.KEY_q, Gdk.KEY_Q, ):
		Gtk.main_quit()
	return True

R_COLOR = 0xe7
G_COLOR = 0xff
B_COLOR = 0xb5
bg_colors = { hwloc.OBJ_SYSTEM: (0xff, 0xff, 0xff),
			  hwloc.OBJ_MACHINE: (R_COLOR, G_COLOR, B_COLOR),
			  hwloc.OBJ_NODE: (0xef, 0xdf, 0xde),
			  hwloc.OBJ_SOCKET: (0xde, 0xde, 0xde),
			  hwloc.OBJ_CACHE: (0xff, 0xff, 0xff),
			  hwloc.OBJ_CORE: (0xbe, 0xbe, 0xbe),
			  hwloc.OBJ_GROUP: (0xff, 0xff, 0xff),
			  hwloc.OBJ_MISC: (0xff, 0xff, 0xff),
			  }

pu_colors = { 'thread': (0xff, 0xff, 0xff),
			  'running': (0, 0xff, 0),
			  'forbidden': (0xff, 0, 0),
			  'offline': (0, 0, 0),
			  }

def obj_string(obj, verbose=0):
	idx = ''
	if obj.os_index != hwloc.UINT_MAX and obj.depth != 0:
		idx = ' P#%u' % (obj.os_index,)
	attr = obj.attr_asprintf(' ', verbose)
	if attr:
		s = '%s%s (%s)' % (obj.type_asprintf(verbose), idx, attr)
	else:
		s = '%s%s' % (obj.type_asprintf(verbose), idx)
	return s

class SomeFrame(Gtk.Frame):
	def __init__(self, obj, child):
		Gtk.Frame.__init__(self)
		self.set_border_width(3)
		self.modify_bg(Gtk.StateType.NORMAL, Gdk.Color(0xffff, 0xffff, 0xffff))
		self.eventbox = Gtk.EventBox()
		self.add(self.eventbox)
		try:
			r, g, b = bg_colors[obj.type]
			self.eventbox.modify_bg(Gtk.StateType.NORMAL, Gdk.Color(r*256, g*256, b*256))
		except:
			pass
		l = Gtk.Label(obj_string(obj))
		l.set_justify(Gtk.Justification.LEFT)
		l.set_alignment(0.0, 0.5)
		l.set_tooltip_text(obj_string(obj, 1))
		if child:
			self.vbox = Gtk.VBox(spacing=5)
			self.vbox.pack_start(l, True, True, 3)
			self.vbox.pack_start(child, True, True, 3)
			self.eventbox.add(self.vbox)
		else:
			self.eventbox.add(l)

	def pack_start(self, widget):
		return self.vbox.pack_start(widget, True, True, 3)

class PUFrame(SomeFrame):
	def __init__(self, obj, child):
		SomeFrame.__init__(self, obj, child)
		if obj.online_cpuset.isset(obj.os_index):
			if not obj.allowed_cpuset.isset(obj.os_index):
				color = 'forbidden'
			else:
				if pid:
					bind = topo.get_proc_cpubind(pid, 0)
				else:
					bind = topo.get_cpubind(0)
				if bind.isset(obj.os_index):
					color = 'running'
				else:
					color = 'thread'
		r, g, b = pu_colors[color]
		self.eventbox.modify_bg(Gtk.StateType.NORMAL, Gdk.Color(r*256, g*256, b*256))


boxes = { hwloc.OBJ_PU: PUFrame,
		  }

def box(obj, child):
	try:
		cls = boxes[obj.type]
	except:
		cls = SomeFrame
	return cls(obj, child)

def add_children(obj):
	# kludgy, kludgy
	if obj.depth < topo.depth//2:
		a, d = rectangle(obj.arity)
	else:
		d, a = rectangle(obj.arity)
	table = Gtk.Table(d, a, False)
	i = 0
	for c in obj.children:
		if c.arity:
			child = add_children(c)
			if not child:
				continue
		else:
			if c.type in [hwloc.OBJ_PCI_DEVICE, hwloc.OBJ_BRIDGE]:
				continue
			child = None
		v = box(c, child)
		table.attach(v, i % a, i % a + 1, i // a, i // a + 1, xpadding=2, ypadding=2)
		i += 1
	if i == 0:
		return None
	return table

progname = sys.argv[0]
def usage(where):
	print('Usage:', progname, '''[ options ]
Options:
  --ignore <type>		Ignore objects of the given type
  --no-caches			Do not show caches
  --no-useless-caches	Do not show caches which do not have a hierarchical
						impact
  --merge				Do not show levels that do not have a hierarcical
						impact
  --no-io				Do not show and I/O devices or bridges
  --pid <pid>			Detect topology as seen by process <pid>
  --whole-system		Do not consider administration limitations
  --version				Report version and exit''', file=where)

try:
	options, args = getopt.getopt(sys.argv[1:], 'h?',
								  [ 'ignore=',
									'no-caches', 'no-useless-caches',
									'merge',
									'no-io',
									'pid=',
									'whole-system',
									'version',
									'help',])
except getopt.GetoptError as err:
	print(str(err), file=sys.stderr)
	usage(sys.stderr)
	sys.exit(2)

pid = None
topo = hwloc.Topology()

show_io = True
whole_system = False

for o, a in options:
	if o == '--version':
		print(progname, hwloc.version_string())
		sys.exit(0)
	elif o in ('-h', '-?', '--help'):
		usage(sys.stdout)
		sys.exit(0)
	elif o == '--ignore':
		try:
			topo.ignore_type(a)
		except hwloc.ArgError as err:
			print(str(err), file=sys.stderr)
			sys.exit(-1)
	elif o == '--no-caches':
		topo.ignore_type(hwloc.OBJ_CACHE)
	elif o == '--no-useless-caches':
		topo.ignore_type_keep_structure(hwloc.OBJ_CACHE)
	elif o == '--merge':
		topo.ignore_all_keep_structure()
	elif o == '--no-io':
		show_io = False
	elif o == '--pid':
		try:
			pid = int(a, 10)
		except ValueError as err:
			print(str(err), '\n', file=sys.stderr)
			usage(sys.stderr)
			sys.exit(-1)
	elif o == '--whole-system':
		whole_system == True

if show_io or whole_system:
	flags = 0
	if whole_system:
		flags |= hwloc.TOPOLOGY_FLAG_WHOLE_SYSTEM
	if show_io:
		flags |= hwloc.TOPOLOGY_FLAG_IO_DEVICES
	topo.set_flags(flags)

topo.load()

window = Gtk.Window()

window.connect('delete_event', delete_event)
window.connect('destroy', destroy)
window.connect('key-press-event', keypress)
window.set_border_width(4)

root = topo.root_obj
b = box(root, add_children(root))
b.set_border_width(3)

if topo.is_thissystem:
	b.pack_start(Gtk.Label('Host: ' + gethostname()))

b.pack_start(Gtk.Label('Indexes: physical'))
b.pack_start(Gtk.Label(datetime.today().strftime('Date: %c')))

sc = Gtk.ScrolledWindow()
sc.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
sc.add_with_viewport(b)
v = b.get_ancestor(Gtk.Viewport)
b.show_all()

window.add(sc)

r = v.size_request()
# kludgy, kludgy
wmax, hmax = rectangle(204800)
w = min(r.width, wmax)+20
h = min(r.height, hmax)+40
window.set_default_size(w, h)

window.show_all()
Gtk.main()
