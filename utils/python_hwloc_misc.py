# -*- python -*-

#
# Copyright (C) 2014-2017 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# misc helpers for hwloc-calc.py and hwloc-bind.py
#

from __future__ import print_function
from os import path

class MiscError(Exception):
	def __init__(self, msg, arg=None):
		self.msg = msg
		self.arg = arg
	def __str__(self):
		try:
			return self.msg % (self.arg,)
		except:
			return self.msg

def input_format_usage(addspaces):
	spc = ' '*addspaces
	return """\n  --input <XML file>
  -i <XML file>   """+spc+"""Read topology from XML file <path>
  --input <directory>
  -i <directory>  """+spc+"""Read topology from chroot containing the /proc and /sys
                  """+spc+"""of another system
  --input \"n:2 2"
  -i \"n:2 2\"      """+spc+"""Simulate a fake hierarchy, here with 2 NUMA nodes of 2
                  """+spc+"""processors
  --input-format <format>
  --if <format>   """+spc+"""Enforce input format among xml, fsroot, synthetic"""


INPUT_DEFAULT, INPUT_XML, INPUT_FSROOT, INPUT_SYNTHETIC = range(4)

_input_format = {'default' : INPUT_DEFAULT,
				 'xml' : INPUT_XML,
				 'fsroot' : INPUT_FSROOT,
				 'synthetic' : INPUT_SYNTHETIC,
				 }

_input_format_1_0 = {'--xml' : INPUT_XML,
					 '--fsys-root' : INPUT_FSROOT,
					 '--synthetic' : INPUT_SYNTHETIC,
					 }

def _handle_input(_option, arg):
	return None, arg

def _handle_input_format(option, arg):
	try:
		return _input_format[arg], None
	except:
		try:
			return _input_format_1_0[option], arg
		except:
			raise MiscError('input format "%s" is not supported', arg)

_input_func_short = {'-i' : _handle_input, }

_input_func_long = {'--input' : _handle_input,
					'--input-format' : _handle_input_format,
					'--if' : _handle_input_format,
					}

_input_func_1_0 = {'--synthetic': _handle_input_format,
				   '--xml' : _handle_input_format,
				   '--fsys_root' : _handle_input_format,
				   }

input_funcs = _input_func_long.copy()
input_funcs.update(_input_func_1_0)

input_format_long = [o[2:]+'=' for o in list(input_funcs.keys())]
input_format_short = 'i:'

input_funcs.update(_input_func_short)

def enable_input_format(topo, input, input_format, verbose):
	if input_format == INPUT_DEFAULT:
		if not path.lexists(input):
			if verbose:
				print('assuming', input, 'is a synthetic topology description')
			input_format = INPUT_SYNTHETIC
		elif path.isdir(input):
			if verbose:
				print('assuming', input, 'is a file-system root')
			input_format = INPUT_FSROOT
		elif path.isfile(input):
			if verbose:
				print('assuming', input, 'is a XML file')
			input_format = INPUT_XML
		else:
			raise MiscError('Unrecognized input file: %s', input)

	if input_format == INPUT_XML:
		if input == '-':
			input = '/dev/stdin'
		topo.set_xml(input)
	elif input_format == INPUT_FSROOT:
		topo.set_fsroot(input)
	elif input_format == INPUT_SYNTHETIC:
		topo.set_synthetic(input)
	else:
		assert False
