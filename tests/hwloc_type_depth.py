#!/usr/bin/env python3
# -*- python -*-

#
# Copyright (C) 2011-2017 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_type_depth.c
# from the hwloc package.
#

import hwloc

SYNTHETIC_TOPOLOGY_DESCRIPTION = 'machine:3 group:2 group:2 core:3 cache:2 cache:2 2'

topo = hwloc.Topology()
topo.set_synthetic(SYNTHETIC_TOPOLOGY_DESCRIPTION)
topo.load()

assert topo.depth == 8

assert topo.get_depth_type(0) == hwloc.OBJ_SYSTEM
assert topo.get_depth_type(1) == hwloc.OBJ_MACHINE
assert topo.get_depth_type(2) == hwloc.OBJ_GROUP
assert topo.get_depth_type(3) == hwloc.OBJ_GROUP
assert topo.get_depth_type(4) == hwloc.OBJ_CORE
assert topo.get_depth_type(5) == hwloc.OBJ_CACHE
assert topo.get_depth_type(6) == hwloc.OBJ_CACHE
assert topo.get_depth_type(7) == hwloc.OBJ_PU

assert topo.get_type_depth(hwloc.OBJ_MACHINE) == 1
assert topo.get_type_depth(hwloc.OBJ_CORE) == 4
assert topo.get_type_depth(hwloc.OBJ_PU) == 7

assert topo.get_type_depth(hwloc.OBJ_NODE) == hwloc.TYPE_DEPTH_UNKNOWN
assert topo.get_type_or_above_depth(hwloc.OBJ_NODE) == 3
assert topo.get_type_or_below_depth(hwloc.OBJ_NODE) == 4
assert topo.get_type_depth(hwloc.OBJ_SOCKET) == hwloc.TYPE_DEPTH_UNKNOWN
assert topo.get_type_or_above_depth(hwloc.OBJ_SOCKET) == 3
assert topo.get_type_or_below_depth(hwloc.OBJ_SOCKET) == 4
assert topo.get_type_depth(hwloc.OBJ_CACHE) == hwloc.TYPE_DEPTH_MULTIPLE
assert topo.get_type_or_above_depth(
    hwloc.OBJ_CACHE) == hwloc.TYPE_DEPTH_MULTIPLE
assert topo.get_type_or_below_depth(
    hwloc.OBJ_CACHE) == hwloc.TYPE_DEPTH_MULTIPLE
