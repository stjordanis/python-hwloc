#!/usr/bin/env python3
# -*- python -*-

#
# Copyright (C) 2011-2017 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_insert_misc.c
# from the hwloc package.
#

import hwloc

topo = hwloc.Topology()
topo.load()
topo.check()
cpuset = hwloc.Bitmap.alloc()
cpuset.set(0)
obj = topo.insert_misc_object_by_cpuset(cpuset, 'test')
topo.insert_misc_object_by_parent(obj, 'test2')
topo.check()
del topo
