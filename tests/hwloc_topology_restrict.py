#!/usr/bin/env python3
# -*- python -*-

#
# Copyright (C) 2011-2017 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_topology_restrict.c
# from the hwloc package.
#

from __future__ import print_function
import hwloc
import errno


def check(nbnodes, nbcores, nbpus):

    # sanity checks
    depth = topology.depth
    assert depth == 4
    depth = topology.get_type_depth(hwloc.OBJ_NODE)
    assert depth == 1
    depth = topology.get_type_depth(hwloc.OBJ_CORE)
    assert depth == 2
    depth = topology.get_type_depth(hwloc.OBJ_PU)
    assert depth == 3

    # actual checks
    nb = topology.get_nbobjs_by_type(hwloc.OBJ_NODE)
    assert nb == nbnodes
    nb = topology.get_nbobjs_by_type(hwloc.OBJ_CORE)
    assert nb == nbcores
    nb = topology.get_nbobjs_by_type(hwloc.OBJ_PU)
    assert nb == nbpus
    total_memory = topology.root_obj.memory.total_memory
    assert total_memory == nbnodes * 1024 * 1024 * \
        1024  # synthetic topology puts 1GB per node


def check_distances(nbnodes, nbcores):

    # node distance
    distance = topology.get_whole_distance_matrix_by_type(hwloc.OBJ_NODE)
    if nbnodes >= 2:
        assert distance
        assert distance.nbobjs == nbnodes
    else:
        assert not distance

    # core distance
    distance = topology.get_whole_distance_matrix_by_type(hwloc.OBJ_CORE)
    if nbnodes >= 2:
        assert distance
        assert distance.nbobjs == nbcores
    else:
        assert not distance


def node_values():
    for i in range(3):
        for j in range(3):
            if i == j:
                yield 10.0
            else:
                yield 20.0


def core_values():
    for i in range(6):
        for j in range(6):
            if i == j:
                yield 4.0
            else:
                yield 8.0

topology = hwloc.Topology()
topology.set_synthetic('node:3 core:2 pu:4')
topology.set_distance_matrix(
    hwloc.OBJ_NODE, list(range(3)), list(node_values()))
topology.set_distance_matrix(
    hwloc.OBJ_CORE, list(range(6)), list(core_values()))
topology.load()

cpuset = hwloc.Bitmap.alloc()

# entire topology
print('starting from full topology')
check(3, 6, 24)
check_distances(3, 6)

# restrict to nothing, impossible
print('restricting to nothing, must fail')
try:
    topology.restrict(cpuset, hwloc.RESTRICT_FLAG_ADAPT_DISTANCES)
    assert False
except OSError as err:
    assert err.errno == errno.EINVAL
check(3, 6, 24)
check_distances(3, 6)

# restrict to everything, will do nothing
print('restricting to everything, does nothing')
# will throw an exception on error
cpuset.fill()
topology.restrict(cpuset, hwloc.RESTRICT_FLAG_ADAPT_DISTANCES)
check(3, 6, 24)
check_distances(3, 6)

# remove a single pu (second PU of second core of second node)
print('removing one PU')
cpuset.fill()
cpuset.clr(13)
# will throw an exception on error
topology.restrict(cpuset, hwloc.RESTRICT_FLAG_ADAPT_DISTANCES)
check(3, 6, 23)
check_distances(3, 6)

# remove the entire second core of first node
print('removing one core')
cpuset.fill()
cpuset.clr_range(4, 7)
# will throw an exception on error
topology.restrict(cpuset, hwloc.RESTRICT_FLAG_ADAPT_DISTANCES)
check(3, 5, 19)
check_distances(3, 5)

# remove the entire third node
print('removing one node')
cpuset.fill()
cpuset.clr_range(16, 23)
# will throw an exception on error
topology.restrict(cpuset, hwloc.RESTRICT_FLAG_ADAPT_DISTANCES)
check(2, 3, 11)
check_distances(2, 3)

# restrict to the third node, impossible
print('restricting to only some already removed node, must fail')
cpuset.zero()
cpuset.set_range(16, 23)
try:
    topology.restrict(cpuset, hwloc.RESTRICT_FLAG_ADAPT_DISTANCES)
    assert False
except OSError as err:
    assert err.errno == errno.EINVAL
check(2, 3, 11)
check_distances(2, 3)

# only keep three PUs (first and last of first core, and last of last core
# of second node)
print('restricting to 3 PUs')
cpuset.zero()
cpuset.set((0, 3, 15))
# will throw an exception on error
topology.restrict(cpuset)
check(2, 2, 3)
check_distances(0, 0)

# only keep one PU (last of last core of second node)
print('restricting to a single PU')
cpuset.zero()
cpuset.set(15)
# will throw an exception on error
topology.restrict(cpuset)
check(1, 1, 1)
check_distances(0, 0)

# check that restricting exactly on a Misc object keeps things coherent
print('restricting to a Misc covering only one of the PU level')
topology = hwloc.Topology()
topology.set_synthetic('pu:4')
topology.load()
cpuset.zero()
cpuset.set_range(1, 2)
topology.insert_misc_object_by_cpuset(cpuset, 'toto')
topology.restrict(cpuset, 0)
topology.check()
