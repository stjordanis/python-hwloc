#!/usr/bin/env python3
# -*- python -*-

#
# Copyright (C) 2011-2017 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_bitmap_singlify.c from
# the hwloc package.
#

from __future__ import print_function
import hwloc


def check_cpuset(myset, expected):
    set2 = hwloc.Bitmap.alloc()
    string = str(myset)
    if expected:
        assert string == expected
    set2.sscanf(string)
    assert myset == set2

    string = myset.list_asprintf()
    set2.list_sscanf(string)
    assert myset == set2

    string = myset.taskset_asprintf()
    set2.taskset_sscanf(string)
    assert myset == set2

# check an empty cpuset
set1 = hwloc.Bitmap.alloc()
check_cpuset(set1, '0x0')
print('empty cpuset converted back and forth, ok')

# check a full (and infinite) cpuset
set1 = hwloc.Bitmap.alloc_full()
check_cpuset(set1, '0xf...f')
print('full cpuset converted back and forth, ok')

# check an infinite (but non full) cpuset
set1.clr(173)
set1.clr_range(60, 70)
check_cpuset(set1, None)
print('infinite/nonfull cpuset converted back and forth, ok')

# check a finite cpuset
set1 = hwloc.Bitmap.alloc()
set1.set(2)
set1.set_range(67, 70)
check_cpuset(set1, '0x00000078,,0x00000004')
print('finite/nonnull cpuset converted back and forth, ok')

topo = hwloc.Topology()
topo.set_synthetic('6 5 4 3 2')
topo.load()
depth = topo.depth

obj = topo.root_obj
string = str(obj.cpuset)
stringlen = len(string)
print('system cpuset is ', string)
check_cpuset(obj.cpuset, None)
print('system cpuset converted back and forth, ok')

# most of the snprintf tests are pointless. I've replaced snprintf with
# asprintf in the python bindings

obj = topo.get_obj_by_depth(depth - 1, 0)
string = hwloc.Obj.cpuset_asprintf((obj,))
print('first cpu cpuset is', string)
check_cpuset(obj.cpuset, None)
print('first cpu cpuset converted back and forth, ok')

obj = topo.get_obj_by_depth(depth - 1, topo.get_nbobjs_by_depth(depth - 1) - 1)
string = hwloc.Obj.cpuset_asprintf((obj,))
print('last cpu cpuset is', string)
check_cpuset(obj.cpuset, None)
print('last cpu cpuset converted back and forth, ok')
