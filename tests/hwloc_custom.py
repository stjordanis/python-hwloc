#!/usr/bin/env python3
# -*- python -*-

#
# Copyright (C) 2011-2017 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of tests/hwloc_custom.c from
# the hwloc package.
#

from __future__ import print_function
import hwloc

print('Loading the local topology...')
loctop = hwloc.Topology()
loctop.set_synthetic('node:2 socket:2 cache:1 core:2 cache:2 pu:2')
loctop.load()

print('Try to create an empty custom topology...')
globtop = hwloc.Topology()
globtop.set_custom()
try:
    globtop.load()
    assert(False)
except:
    pass

print('Creating a custom topology...')
globtop = hwloc.Topology()
globtop.set_custom()

print('Inserting the local topology into the global one...')
root = globtop.root_obj

sw1 = globtop.custom_insert_group_object_by_parent(root, 0)
sw11 = globtop.custom_insert_group_object_by_parent(sw1, 1)
globtop.custom_insert_topology(sw11, loctop, None)
globtop.custom_insert_topology(sw11, loctop, None)
sw12 = globtop.custom_insert_group_object_by_parent(sw1, 1)
globtop.custom_insert_topology(sw12, loctop, None)
globtop.custom_insert_topology(sw12, loctop, None)

sw2 = globtop.custom_insert_group_object_by_parent(root, 0)
sw21 = globtop.custom_insert_group_object_by_parent(sw2, 1)
globtop.custom_insert_topology(sw21, loctop, None)
globtop.custom_insert_topology(sw21, loctop, None)
globtop.custom_insert_topology(sw21, loctop, None)
sw22 = globtop.custom_insert_group_object_by_parent(sw2, 1)
# only one to check that it won't get merged
globtop.custom_insert_topology(sw22, loctop, None)

loctop = None

print('Building the global topology...')
globtop.load()
globtop.check()

assert globtop.depth == 10
assert globtop.get_depth_type(0) == hwloc.OBJ_SYSTEM
assert globtop.get_nbobjs_by_type(hwloc.OBJ_SYSTEM) == 1
assert globtop.get_depth_type(1) == hwloc.OBJ_GROUP
assert globtop.get_nbobjs_by_depth(1) == 2
assert globtop.get_depth_type(2) == hwloc.OBJ_GROUP
# the last group of this level shouldn't be merged
assert globtop.get_nbobjs_by_depth(2) == 4
assert globtop.get_depth_type(3) == hwloc.OBJ_MACHINE
assert globtop.get_nbobjs_by_type(hwloc.OBJ_MACHINE) == 8
assert globtop.get_depth_type(4) == hwloc.OBJ_NODE
assert globtop.get_nbobjs_by_type(hwloc.OBJ_NODE) == 16
assert globtop.get_depth_type(5) == hwloc.OBJ_SOCKET
assert globtop.get_nbobjs_by_type(hwloc.OBJ_SOCKET) == 32
assert globtop.get_depth_type(6) == hwloc.OBJ_CACHE
assert globtop.get_nbobjs_by_depth(6) == 32
assert globtop.get_depth_type(7) == hwloc.OBJ_CORE
assert globtop.get_nbobjs_by_type(hwloc.OBJ_CORE) == 64
assert globtop.get_depth_type(8) == hwloc.OBJ_CACHE
assert globtop.get_nbobjs_by_depth(8) == 128
assert globtop.get_depth_type(9) == hwloc.OBJ_PU
assert globtop.get_nbobjs_by_type(hwloc.OBJ_PU) == 256
