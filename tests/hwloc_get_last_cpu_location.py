#!/usr/bin/env python3
# -*- python -*-

#
# Copyright (C) 2011-2017 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_get_last_cpu_location.c
# from the hwloc package.
#

import hwloc

# check that a bound process execs on a non-empty cpuset included in the
# binding


def check(cpuset, flags):
    topology.set_cpubind(cpuset, flags)

    last = topology.get_last_cpu_location(flags)

    assert not last.iszero
    assert last in cpuset


def checkall(cpuset):
    if support.cpubind.get_thisthread_last_cpu_location:
        check(cpuset, hwloc.CPUBIND_THREAD)
    if support.cpubind.get_thisproc_last_cpu_location:
        check(cpuset, hwloc.CPUBIND_PROCESS)
    if support.cpubind.get_thisthread_last_cpu_location \
            or support.cpubind.get_thisproc_last_cpu_location:
        check(cpuset, 0)

topology = hwloc.Topology()
topology.load()
support = topology.support

# check at top level
obj = topology.root_obj
checkall(obj.cpuset)

depth = topology.depth
# check at intermediate level if it exists
if depth >= 3:
    for o in topology.objs_by_depth((depth - 1) / 2):
        checkall(o.cpuset)
# check at bottom level
for o in topology.objs_by_type(hwloc.OBJ_PU):
    checkall(o.cpuset)
