#!/usr/bin/env python3
# -*- python -*-

#
# Copyright (C) 2011-2017 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_synthetic.c
# from the hwloc package.
#

import hwloc

# check a synthetic topology
topo = hwloc.Topology()
topo.set_synthetic('2 3 4 5 6')
topo.load()

# internal checks

topo.check()

# local checks
assert topo.depth == 6

width = 1
for i in range(6):
    assert topo.get_nbobjs_by_depth(i) == width
    for j in range(width):
        obj = topo.get_obj_by_depth(i, j)
        assert obj
        a = 0
        if i < 5:
            a = i + 2
        assert obj.arity == a
    width *= i + 2

buffer_ = topo.export_synthetic(0)
assert len(buffer_) == 75
assert buffer_ == 'NUMANode:2(memory=1073741824) Package:3 L2Cache:4(size=4194304) Core:5 PU:6'

buffer_ = topo.export_synthetic(
    hwloc.TOPOLOGY_EXPORT_SYNTHETIC_FLAG_NO_EXTENDED_TYPES | hwloc.TOPOLOGY_EXPORT_SYNTHETIC_FLAG_NO_ATTRS)
assert len(buffer_) == 40
assert buffer_ == 'NUMANode:2 Package:3 Cache:4 Core:5 PU:6'

topo = None

topo = hwloc.Topology()
topo.set_flags(hwloc.TOPOLOGY_FLAG_ICACHES)
topo.set_synthetic(
    'sock:2(indexes=3,5) numa:2(memory=262144 indexes=sock) l3u:1 l2:2 l1i:1(size=16384) l1dcache:2 core:1 pu:2(indexes=l2)')
topo.load()

buffer_ = topo.export_synthetic(0)
assert len(buffer_) == 175
assert buffer_ == 'Package:2 NUMANode:2(memory=262144 indexes=2*2:1*2) L3Cache:1(size=16777216) L2Cache:2(size=4194304) L1iCache:1(size=16384) L1dCache:2(size=32768) Core:1 PU:2(indexes=4*8:1*4)'

assert topo.get_obj_by_type(hwloc.OBJ_SOCKET, 1).os_index == 5
assert topo.get_obj_by_type(hwloc.OBJ_NODE, 1).os_index == 2
assert topo.get_obj_by_type(hwloc.OBJ_PU, 12).os_index == 3
assert topo.get_obj_by_type(hwloc.OBJ_PU, 13).os_index == 11
assert topo.get_obj_by_type(hwloc.OBJ_PU, 14).os_index == 19
assert topo.get_obj_by_type(hwloc.OBJ_PU, 15).os_index == 27
assert topo.get_obj_by_type(hwloc.OBJ_PU, 16).os_index == 4
assert topo.get_obj_by_type(hwloc.OBJ_PU, 17).os_index == 12
assert topo.get_obj_by_type(hwloc.OBJ_PU, 18).os_index == 20
assert topo.get_obj_by_type(hwloc.OBJ_PU, 19).os_index == 28

topo = None

topo = hwloc.Topology()
topo.set_synthetic('sock:2 core:2 pu:2(indexes=0,4,2,6,1,5,3,7)')
topo.load()

buffer_ = topo.export_synthetic(0)
assert len(buffer_) == 42
assert buffer_ == 'Package:2 Core:2 PU:2(indexes=4*2:2*2:1*2)'

assert topo.get_obj_by_type(hwloc.OBJ_PU, 0).os_index == 0
assert topo.get_obj_by_type(hwloc.OBJ_PU, 1).os_index == 4
assert topo.get_obj_by_type(hwloc.OBJ_PU, 2).os_index == 2
assert topo.get_obj_by_type(hwloc.OBJ_PU, 3).os_index == 6
assert topo.get_obj_by_type(hwloc.OBJ_PU, 4).os_index == 1
assert topo.get_obj_by_type(hwloc.OBJ_PU, 5).os_index == 5
assert topo.get_obj_by_type(hwloc.OBJ_PU, 6).os_index == 3
assert topo.get_obj_by_type(hwloc.OBJ_PU, 7).os_index == 7

topo = None

topo = hwloc.Topology()
topo.set_synthetic('sock:2 core:2 pu:2(indexes=0,4,2,6,1,3,5,7)')
topo.load()

buffer_ = topo.export_synthetic(0)
assert len(buffer_) == 46

assert topo.get_obj_by_type(hwloc.OBJ_PU, 0).os_index == 0
assert topo.get_obj_by_type(hwloc.OBJ_PU, 1).os_index == 4
assert topo.get_obj_by_type(hwloc.OBJ_PU, 2).os_index == 2
assert topo.get_obj_by_type(hwloc.OBJ_PU, 3).os_index == 6
assert topo.get_obj_by_type(hwloc.OBJ_PU, 4).os_index == 1
assert topo.get_obj_by_type(hwloc.OBJ_PU, 5).os_index == 3
assert topo.get_obj_by_type(hwloc.OBJ_PU, 6).os_index == 5
assert topo.get_obj_by_type(hwloc.OBJ_PU, 7).os_index == 7

topo = None
