#!/usr/bin/env python3
# -*- python -*-

#
# Copyright (C) 2011-2017 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_groups.c
# from the hwloc package.
#

import hwloc

topology = hwloc.Topology()
topology.set_synthetic('node:3 pu:1')

#  group 3 numa nodes as 1 group of 2 and 1 on the side
indexes = [0, 1, 2]
distances = [1.0, 4.0, 4.0,
             4.0, 1.0, 2.0,
             4.0, 2.0, 1.0]
topology.set_distance_matrix(hwloc.OBJ_PU, indexes, distances)

topology.load()

# 2 groups at depth 1
depth = topology.get_type_depth(hwloc.OBJ_GROUP)
assert depth == 1
width = topology.get_nbobjs_by_depth(depth)
assert width == 1

# 3 nodes at depth 2
depth = topology.get_type_depth(hwloc.OBJ_NODE)
assert depth == 2
width = topology.get_nbobjs_by_depth(depth)
assert width == 3

# find the root obj
obj = topology.root_obj
assert obj.arity == 2

# check its children
kids = list(obj.children)
assert kids[0].type == hwloc.OBJ_NODE
assert kids[0].depth == 2
assert kids[0].arity == 1
assert kids[1].type == hwloc.OBJ_GROUP
assert kids[1].depth == 1
assert kids[1].arity == 2

del indexes
del distances
del topology

# group 5 sockets as 2 group of 2 and 1 on the side, all of them below a
# common node object
topology = hwloc.Topology()
topology.set_synthetic('node:1 socket:5 pu:1')

indexes = [0, 1, 2, 3, 4]
distances = [1.0, 2.0, 4.0, 4.0, 4.0,
             2.0, 1.0, 4.0, 4.0, 4.0,
             4.0, 4.0, 1.0, 4.0, 4.0,
             4.0, 4.0, 4.0, 1.0, 2.0,
             4.0, 4.0, 4.0, 2.0, 1.0
             ]
topology.set_distance_matrix(hwloc.OBJ_SOCKET, indexes, distances)
topology.load()

# 1 node at depth 1
depth = topology.get_type_depth(hwloc.OBJ_NODE)
assert depth == 1
width = topology.get_nbobjs_by_depth(depth)
assert width == 1

# 2 groups at depth 2
depth = topology.get_type_depth(hwloc.OBJ_GROUP)
assert depth == 2
width = topology.get_nbobjs_by_depth(depth)
assert width == 2

# 5 sockets at depth 3
depth = topology.get_type_depth(hwloc.OBJ_SOCKET)
assert depth == 3
width = topology.get_nbobjs_by_depth(depth)
assert width == 5

# find the node obj
obj = topology.root_obj
assert obj.arity == 1
obj = obj.first_child
assert obj.type == hwloc.OBJ_NODE
assert obj.arity == 3

# check its children
kids = list(obj.children)
obj = kids[0]
assert obj.type == hwloc.OBJ_GROUP
assert obj.depth == 2
assert obj.arity == 2
obj = kids[1]
assert obj.type == hwloc.OBJ_SOCKET
assert obj.depth == 3
assert obj.arity == 1
obj = kids[2]
assert obj.type == hwloc.OBJ_GROUP
assert obj.depth == 2
assert obj.arity == 2
