#!/usr/bin/env python3
# -*- python -*-

#
# Copyright (C) 2011-2017 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_distances.c
# from the hwloc package.
#

from __future__ import print_function
import hwloc
import os
import sys


def print_distances(distances):
    nbobjs = distances.nbobjs
    latency = distances.latency

    # column header
    print('     ', end=' ')
    for j in range(nbobjs):
        print('% 5d' % j, end=' ')
    print()

    # each line
    for i in range(nbobjs):
        # row header
        print('% 5d' % i, end=' ')
        for j in range(nbobjs):
            print('%2.3f' % latency[i * nbobjs + j], end=' ')
        print()

topology = hwloc.Topology()
topology.set_synthetic('node:4 core:4 pu:1')
os.environ['HWLOC_NUMANode_DISTANCES'] = '0,1,2,3:2*2'
os.environ['HWLOC_PU_DISTANCES'] = '0-15:4*2*2'
topology.load()

topodepth = topology.depth

for depth in range(topodepth):
    distances = topology.get_whole_distance_matrix_by_depth(depth)
    if not distances or not distances.latency:
        print('No distance at depth', depth, file=sys.stderr)
        continue

    print('distance matrix for depth %u:' % depth)
    print_distances(distances)
    nbobjs = distances.nbobjs

    obj1 = topology.get_obj_by_depth(depth, 0)
    obj2 = topology.get_obj_by_depth(depth, nbobjs - 1)
    try:
        d1, d2 = topology.get_latency(obj1, obj2)
    except hwloc.ArgError as msg:
        print(msg)
        assert False
    assert d1 == distances.latency[0 * nbobjs + (nbobjs - 1)]
    assert d2 == distances.latency[(nbobjs - 1) * nbobjs + 0]

# check that get_latency works fine on numa distances
distances = topology.get_whole_distance_matrix_by_type(hwloc.OBJ_NODE)
if not distances or not distances.latency:
    print('No NUMA distance matrix!', file=sys.stderr)
    assert False

print('distance matrix for NUMA nodes')
print_distances(distances)
nbobjs = distances.nbobjs

for i in range(nbobjs):
    for j in range(nbobjs):
        obj1 = topology.get_obj_by_type(hwloc.OBJ_NODE, i)
        obj2 = topology.get_obj_by_type(hwloc.OBJ_NODE, j)
        try:
            d1, d2 = topology.get_latency(obj1, obj2)
        except hwloc.ArgError as msg:
            print(msg)
            assert False
        assert d1 == distances.latency[i * nbobjs + j]
        assert d2 == distances.latency[j * nbobjs + i]
# check that some random vlaues are ok
assert distances.latency[0] == 1.0  # diagonal
assert distances.latency[4] == 4.0  # same group
assert distances.latency[6] == 8.0  # different group
assert distances.latency[9] == 8.0  # different group
assert distances.latency[10] == 1.0  # diagonal
assert distances.latency[14] == 4.0  # same group

# check that hwloc_get_latency works fine on PU distances
distances = topology.get_whole_distance_matrix_by_type(hwloc.OBJ_PU)
if not distances or not distances.latency:
    print('No PU distance matrix!', file=sys.stderr)
    assert False

print('distance matrix for PU nodes')
print_distances(distances)
nbobjs = distances.nbobjs
for i in range(nbobjs):
    for j in range(nbobjs):
        obj1 = topology.get_obj_by_type(hwloc.OBJ_PU, i)
        obj2 = topology.get_obj_by_type(hwloc.OBJ_PU, j)
        try:
            d1, d2 = topology.get_latency(obj1, obj2)
        except hwloc.ArgError as msg:
            print(msg)
            assert False
        assert d1 == distances.latency[i * nbobjs + j]
        assert d2 == distances.latency[j * nbobjs + i]
# check that some random vlaues are ok
assert distances.latency[0] == 1.0  # diagonal
assert distances.latency[1] == 2.0  # same group
assert distances.latency[3] == 4.0  # same biggroup
assert distances.latency[15] == 8.0  # different biggroup
assert distances.latency[250] == 8.0  # different biggroup
assert distances.latency[253] == 4.0  # same group
assert distances.latency[254] == 2.0  # same biggroup
assert distances.latency[255] == 1.0  # diagonal
